package com.zuitt.example;

import java.util.ArrayList;
import java.util.Arrays;
import  java.util.HashMap;

public class WDC043_S2_A2 {
    public static void main(String [] args){
        int[] primeNumbers = new int[5];

        primeNumbers[0] = 2;
        primeNumbers[1] = 3;
        primeNumbers[2] = 5;
        primeNumbers[3] = 7;
        primeNumbers[4] = 11;

        String[] ordinals = {"first", "second", "third", "fourth", "fifth"};
        for (int i = 0; i < primeNumbers.length; i++){
            System.out.println("The " +  ordinals[i] + " prime number is: " + primeNumbers[i]);
        }

        ArrayList<String> myFriends = new ArrayList<>(Arrays.asList("John","Jane","Chloe","Zoey"));
        System.out.println("My Friends are: " + myFriends);

        HashMap<String, Integer> productInv =new HashMap<String, Integer>();
        
        productInv.put("Toothpaste",15);
        productInv.put("Toothbrush",20);
        productInv.put("Soap",12);
        System.out.println("Our current inventory consists of: " + productInv);
    }
}
